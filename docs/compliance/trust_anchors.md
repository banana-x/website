# :material-anchor: Trust Anchors

!!! tip "TL;DR Trust Anchors definition "

    The Trust Anchors are for attestations, what TSP are for identities.

For a given ecosystem, the ecosystem authority identifies the Trust Anchors as the parties eligible to issue an attestation about a specific subject.


![attestation on wall](images/workflow.png)


The Trust Anchors are parties, ie CAB or technical means, [accredited](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.7) by the ecosystem authority to be trustworthy anchors in the cryptographic chain of public keys used to digitally sign attestations or claims about an object.

For each accredited Trust Anchors, a specific [scope of attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.4) is defined.

The Trust Anchors are not necessarily [Root Certificate Authorities](https://en.wikipedia.org/wiki/Root_certificate) as commonly understood. A Trust Anchor can be relative to another property in a claim.

```mermaid
sequenceDiagram
    autonumber
    participant TA as Trust Anchor
    participant holder as Holder

    TA ->> holder: issues a signed and machine readable attestation
```

The list of valid Trust Anchors and rules are available via the ecosystem Registry.


!!! example "Examples of Trust Anchors"

    === "For a state identity"
        For a sovereign identity, ie a legal or national identity issued by the state, only a state accredited party can be a Trust Anchor. In the specific case of identity, those are named [Trust Service Provider](https://portal.etsi.org/TB-SiteMap/ESI/Trust-Service-Providers).

        _PS: sovereign is here used as defined in the dictionary and doesn't relate to SSI._

    === "For personal data processing"

        For the consent to process personal data outside the scope of legitimate interest, only the data subject can be the Trust Anchor.

## :fontawesome-solid-person-military-pointing: Trusted Data sources and Notaries

When an accredited Trust Anchors is not capable of issuing cryptographic material nor sign claim directly, then the ecosystem authority accredits one or more Notaries which will perform a [validation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5) based on objective evidence from a Trusted Data sources and issue an [attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3) about the previously made attestation from the Trust Anchors.

The Notaries are not performing [audit](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.4) nor [verification](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.6) of the [object of conformity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.2).

The VC signed by such a Notary will be second-party conformity assessment activity and have as `credentialSubject` a statement about the original attestation.  
The overall trust level of such a VC will be lower than if the CAB was able to digitally sign the VC directly without involving a Notary. See the [Veracity Trust Index](trust_indexes.md#veracity)

The Notaries are converting "not machine readable" proofs into "machine readable" proofs.

```mermaid
sequenceDiagram
    autonumber
    participant TA as Trust Anchor
    participant notary as Notary
    participant holder as Holder

    alt attestation comes directly from the trust anchor
      TA ->> notary: issues an unsigned or non-machine readable attestation
    else attestation comes from the holder
      TA ->> holder: issues an unsigned or non-machine readable attestation
      holder ->> notary: presents the unsigned or non-machine readable attestation
    end
    Note over notary: the notary uses Trusted Data Sources to validates the attestation
    notary ->> notary: translates from a non-machine readable to a machine readable and signs
    Note over notary: the notary becomes a Trust Anchor
    notary ->> holder: issues a signed and machine readable attestation
```


!!! example "Examples of Notary"

    The European Commission is providing several API, including one to check the [validity of EORI number](https://ec.europa.eu/taxation_customs/dds2/eos/eori_validation.jsp).  
    Unfortunately, those API are not returning Verifiable Credentials. Hence the development of a notary service for EORI using the European Commission API as Trusted Data source for validation.

### Evidence

It is expected that the VC issued by the Notaries contain the [evidence](https://www.w3.org/TR/vc-data-model-2.0/#evidence) of the validation process.

## :octicons-file-code-24: Implementation

The list of Trust Anchors, Trusted Data Sources and Notaries are discoverable via the ecosystem Registry.

The Registry can implement several interfaces:

- [European Blockchain Service Infrastructure API](https://api-pilot.ebsi.eu/docs/apis)
- a Trust-service Status List (TSL) following [ETSI TS 119 612](https://www.etsi.org/deliver/etsi_ts/119600_119699/119612/02.01.01_60/ts_119612v020101p.pdf) format exposed via:
    - a well-known URL, like the [European Commission TSL](https://ec.europa.eu/tools/lotl/eu-lotl.xml)
    - DNS PTR record, like in the [TRAIN](https://www.hci.iao.fraunhofer.de/de/identity-management/identity-und-accessmanagement/TRAIN_EN.html) model, pointing to a URL (`https://`, `ipfs://`, ...).
    - a smart-contract via blockain RPC nodes
- ...

<!--
TODO

### Gaia-X Trust Framework

Using [Gaia-X Credential](credential.md), the Gaia-X Trust Framework is a policy rules agnostic solution to implement a decentralised governance.

The Gaia-X Trust Framework defines the process of going through and validating the set of automatically enforceable rules to achieve the minimum level of compatibility in terms of:

- syntactic correctness,
- schema validity,
- cryptographic signature validation,
- attribute value consistency,
- attribute value verification.

Whenever possible, the claims validation is done either by using publicly available open data, and performing tests or using data from Trusted Data Sources as defined in the previous section.

The claim are all contained in verifiable credentials, independtly of who is performing the assessment. See the role of [Notaries](#gaia-x-trusted-data-sources-and-gaia-x-notaries) above.


#### Gaia-X Compliance and Gaia-X Label
 
The Gaia-X Compliance and Gaia-X Label are implementation of the Gaia-X governance.

To operationalise them, a software implementation must be executed and turned into up and running services, providing traceable evidence of correct executions.

While the governance of the Gaia-X Compliance rules and process is and will stay under the control of the Gaia-X Association, the Gaia-X Trust Framework services will go through several mutually non-exclusive deployments scenario[^article-compliance-deployment].

[^article-compliance-deployment]: <https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/>

During the pilot phase, the current, non-final deployment is described below.

| Deliverables           | Notes |
|---------------------|-------|
| [Gaia-X Compliance Service](https://compliance.gaia-x.eu)     | This service validates the shape, content and signature of Gaia-X Credentials and issues back a Gaia-X Credential attesting of the result. Required fields and consistency rules are defined in this document. Format of the input and output are defined in the [Identity, Credential and Access management document](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/latest).|
| [Gaia-X Registry](https://registry.gaia-x.eu) | This service provides a list of valid shapes, and valid and revoked public keys. The Gaia-X Registry will also be used as the seeding list for the network of catalogues. More information in the Architecture document: [Gaia-X Registry](https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#gaia-x-decentralized-autonomous-ecosystem). |


### Extention by the dataspaces

The dataspaces can extend the definition of the above defined Gaia-X Trust Anchors, Gaia-X Trusted Data Sources and Gaia-X Notaries as follow:

- a federation can add more requirements on the eligibility of the Gaia-X Trust Anchors, hence selecting a subset of the Gaia-X Trust Anchors for the federation's domain.
- a federation can add additional rules on top of the ones in this document by:
    - adding more criteria for a Self-Description type. Example: adding requirements for a participant to join the federation, enforcing a deeper level of transparency for a Service Offering
    - selecting new domain specific Trust Anchors for the new criteria.

**! warning !**: For an already defined criteria, it will break Gaia-X Compliance to extend the list of Gaia-X Trust Anchors eligible to sign the criteria.

![Ecosystem rules](figures/ecosystem_rules.svg)

$$\mathbb{R}_{GaiaX} \subseteq \mathbb{R}_{domain}$$

| Rule property                                 | A domain refining Gaia-X Compliance <br>$\forall r \in \mathbb{R}_{GaiaX} \text{ and } r_{domain} \in \mathbb{R}_{domain}$ | A domain extending Gaia-X Compliance<br>$\forall r \in \mathbb{R}_{domain} \setminus \mathbb{R}_{GaiaX}$ |
|-----------------------------------------------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| Attribute name: $r_{name}$                           | $r_{name_{CamelCase}} \equiv r_{name_{snake\_case}}$                               | $r_{name} \notin \mathbb{R}_{GaiaX}$                             |
| Cardinality: $\lvert r \lvert$               | $\lvert r_{domain} \lvert \ge \lvert r \lvert$                                     | _no restriction_                                                   |
| Value formats: $r \rightarrow \texttt{VF}(r)$          | $\texttt{VF}(r_{domain}) \subseteq \texttt{VF}(r)$                                       | _no restriction_                                                   |
| Trust Anchors: $r \rightarrow \texttt{TA}(r)$         | $\texttt{TA}(r_{domain}) \subseteq \texttt{TA}(r)$                             | _no restriction_                                                   |
| Trusted Data Sources: $r \rightarrow \texttt{TDS}(r)$ | $\texttt{TDS}(r_{domain}) \subseteq \texttt{TDS}(r)$                 | _no restriction_                                                   |



#### Compliance remediation

Gaia-X Compliance credentials may become invalid over time. There are three states declaring such a credential as invalid:

- Expired (after a timeout date, e.g., the expiry of a cryptographic signature)
- Deprecated (replaced by a newer Self-Description)
- Revoked (by the original issuer or a trusted party, e.g., because it contained incorrect or fraudulent information)

Expired and Deprecated can be deduced automatically based in the information already stored in the Gaia-X Registry or Gaia-X Catalogues. There are no additional processes to define. This section describes how Gaia-X credentials are revoked. 

The importance of Gaia-X compliance will grow over time, covering more and more Gaia-X principles such as interoperability, portability, and security. However, automation alone is not enough and the operating model must include a mechanism to demotivate malicious actors to corrupt the Gaia-X Registry and Gaia-X Catalogues.

The revocation of Gaia-X credentials can be done in various ways:

- **Revocation or Deprecation by authorship**: The author of a Gaia-X credentials revokes or deprecates the credential explicitly.
- **Revocation by automation**: The Gaia-X Compliance Service found at least one credential claims not validating the Gaia-X compliance rules.
- **Suspension and Revocation by manual decision**: After an audit by a compliant Gaia-X Participant, if at least one credential claims is found to be incorrect, the suspension of the Gaia-X credential is automatic. The revocation is submitted for approval to the Gaia-X Association with the opportunity for the credengtial's issuer to state its views in a matter of days. To minimize subjective decisions and promote transparency, the voting results will be visible and stored on the Gaia-X Registry or in the local Ecosystem's Registry.

### Gaia-X Decentralized Autonomous Ecosystem

The operating model described in this chapter motivates the creation of a Gaia-X decentralised autonomous Ecosystem following the principles of a Decentralized Autonomous Organisation[^dao], with the following characteristics:

- Compliance is achieved through a set of automatically enforceable rules whose goal is to incentivize its community members to achieve a shared common mission.
- Maximizing the decentralization at all levels to reduce lock-in and lock-out effects.
- Minimizing the governance and central leadership to minimize liability exposure and regulatory capture.
- The ecosystem has its own rules, including management of its own funds.
- The ecosystem is operated by the ecosystem's Participants

[^dao]: Example of the setup of a DAO <https://blockchainhub.net/dao-decentralized-autonomous-organization/>

:information_source: Other ecosystems are autonomous and this operating model does not enforce how internal ecosystem governance should be handled.

#### Gaia-X Registry

The Gaia-X Registry is a public distributed, non-repudiable, immutable, permissionless database with a decentralised infrastructure and the capacity to automate code execution.

:information_source: The Ecosystems may want to have their own instance of a local Registry or equivalent. Technically, this component can be part of the ecosystem local Catalogues.

The Gaia-X Registry is the backbone of the ecosystem governance which stores information, similarly to the [Official Journal of the European Union](https://eur-lex.europa.eu/oj/direct-access.html), such as:

- the list of the Trust Anchors – keyring.
- the result of the Trust Anchors validation processes.
- the potential revocation of Trust Anchors identity.
- the vote and results of the Gaia-X Association roll call vote, similar to the rules of the [plenary of the European Parliament](https://www.europarl.europa.eu/about-parliament/en/organisation-and-rules/how-plenary-works)
- the URLs of the Gaia-X credentials schemas defined by Gaia-X
- the URLs of Gaia-X Catalogue's credentials
- … 

It also facilitates the provision of:

1. A decentralised network with smart contract functionality.
2. Voting mechanisms that ensure integrity, non-repudiation and confidentiality.
3. Access to a Gaia-X Compliance Service instance.
4. A fully operational, decentralised and easily searchable catalogue[^OP].
5. A list of Participants' identities and Self-Description URIs which violate Gaia-X membership rules. This list must be used by all Gaia-X Trusted Catalogue providers to filter out any inappropriate content.
6. Tokens may cover the operating cost of the Gaia-X Ecosystem. This specific point can be abstracted by 3rd party brokers wrapping token usage with fiat currency, providing opportunities for new services to be created by the Participants. Emitting tokens for the Gaia-X Association's members is also considered.

:information_source: Each entry in the Gaia-X Registry is considered as a transaction. A transaction contains [DID](https://w3c.github.io/did-core/)s of all actors involved in the transaction and metadata about the transaction in a machine readable format.  The basic rule for a transaction to be valid is that all DIDs have one of the Trust Anchors as root Certificate Authorities.  Please also note that the Registry stores all revoked Trust Anchors.

This model enables the Participants to operate in the Gaia-X Ecosystem, to autonomously register information, and to access the information which is verifiable by other Participants.  

[^OP]: Example of decentralised data and algorithms marketplace <https://oceanprotocol.com/>


### Dataspace / Federation onboarding and offboarding

The onboarding and offboarding of entities - participants, services, resources - inside a dataspace or federation is under the responsibility of the dataspace and federation autorities which are autonomous from Gaia-X.

The above onboarding and offboarding processes are convenient optional ways for the participants themselves to request, demonstrate, collect, revoke attributes/properties about themselves, their services, their resources.

Those attributes/properties are expressed and exchanged via [Gaia-X Credentials](credential.md).

Gaia-X doesn't mandate nor enforce a particular timeline to gather Gaia-X credentials.

Those could also be gathered at the time of the contract negotiation, on the fly, depending of the policies being negotiated.

## Policy

## Our building block

The building block of the Gaia-X model is based on policy expression attached to each entity of the model.
The `policies` are expressed by one or more `parties` about one or more `assets`.
An `asset` can also be a `party`, making this simple model recursive and capable to handle the most complicated user-scenario, with multiple providers, consumers, federators, [data intermediaries](https://digital-strategy.ec.europa.eu/en/policies/data-governance-act-explained#ecl-inpage-l4ihlqt9), data subjects, business legal representatives, employer/employee and much more.

```mermaid
flowchart TD
    party(Party)
    policy(Policy)
    rule(Rule)
    asset(Asset)
    action(Action)

%%    party -- expresses --> policy
    policy -- a non-empty set of --> rule
    rule -- ability/inability/obligation to exercise --> action
    action -- is an operation on --> asset
    asset -- subjects to --> rule
    party -- has role in --> rule
    asset -. can also be a .-> party
```

The workflow above is the generic representation of the [Open Digital Rights Language (ODRL)](https://www.w3.org/TR/odrl-model/) model which is used in the rest of this document.

-->