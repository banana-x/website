# :material-ruler: Compliance

The compliance refer to specific guidelines, principles, or regulations that determine how parties operate, makes decisions, and enforces its policies.

The compliance with those policy rules are demonstrated via the fulfilment of [conformity assessment scheme](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.9), which are about rule(s) - the what - and procedure(s) - the how.

The conformity assessment scheme establish consistent and fair frame of references, reusable and extendable by [ecosystem authorities](../ecosystem/authority.md) to quickly operationalise their governance.

More details on the technical implementation in the [Fundamentals/Conformity](../fundamentals/conformity.md) section.

## The Object of Conformity

Challenges to have interoperable conformity scheme are:

* to have machine readable information.
* to adopt an information model and a common vocabulary for performing and describing conformity-related tasks.
* to ensure proper identification of the object of conformity and its scope.

Without those, the output of the assessment, which are attestations, is expected to collect dust, hung on walls.

![attestation on wall](images/conformity wall.jpg)

## Conformity vs Trust Index

While the use of conformity scheme is a very well-established mechanism, an attestation is a fixed point in time evaluation with a boolean value: conformant or non conformant.  
It doesn't accommodate well with a more granular approach.


| Conformity Schema                             | Trust Index                     |
|-----------------------------------------------|---------------------------------|
| Boolean result :fontawesome-solid-toggle-off: | Gradual ranking :material-sort: |
