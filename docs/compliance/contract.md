# :fontawesome-solid-file-contract: Contract


??? warning "This section does not provide legal guidance"

    !!! info inline end "Extract from the book review"

        _Contract law, more than any other field of private law, has been exposed to the force of European integration. This textbook is firmly placed in that context, but reflects as well the conviction that the national legal systems of Europe will not experiment with total harmonisation of the field in the foreseeable future. Hence, to address contract law on the basis of a broad comparative approach will remain a necessity in Europe for all those who do not intend to perpetuate isolationist tendencies._

        (From <https://doi.org/10.1515/ercl-2022-2052>)

    For more information, consider reading the [Contract Law: A Comparative Introduction](https://www.e-elgar.com/shop/gbp/contract-law-9781800373129.html).

    ![](https://www.e-elgar.com/shop/media/catalog/product/cache/01c740ac49768798d3ac9bd0cdac340f/9/7/9781800373105.jpg)

A contract :fontawesome-solid-file-signature: is a legally binding agreement, which covers from example buying a house :fontawesome-solid-house-chimney: in a notarial's office to buying a soda :material-bottle-soda-classic-outline: in a vending machine.

Depending on the scope and type of contract, it's possible to supervise or even automate contracting on:

- The procedures to establish the agreement, such as signature, metadata, storage, ...
- The policy rules and clauses reasoning to reach the agreement, which depends on the scope, the domain and the complexity of the expressed rules.
