# :simple-trustpilot: Trust

This chapter has three objectives:

- help the users of the framework to make educated decisions, independently of the jurisdictions and domains they are operating in.
- be extendable for specific jurisdictions (Japan, $\dots$) and domains needs (finance, health, $\dots$)
- be the starting point for the elaboration of automated compliance.

!!! tip
    The chapter [Fundamentals](../fundamentals/index.md) is not a prerequisite for this chapter, but an overall ICT knowledge is required.

The first important element of that framework is Trust.
In accordance with the commonly accepted English definitions from [Collins](https://www.collinsdictionary.com/dictionary/english/trust), [Cambridge](https://dictionary.cambridge.org/dictionary/english/trust) and [Oxford](https://www.oxfordlearnersdictionaries.com/definition/english/trust), trust is defined in this document as
> trust: the favourable response of a decision-making party who assesses the risk concerning the target party's ability to fulfil a promise.


```mermaid
flowchart LR
    decision(["`decision-marking
    party`"])
    target(["`target
    party`"])
    promise["`promise`"]

    decision -- assesses risk about --> promise -- made by --> target
```

This definition implies the following:

- Trust involves at minima two parties: the decision-making party $p_1$ and the target party $p_2$. 
- Trust is not automatically reciprocal or mutual: the fact that the decision-making party trusts the targeted party doesn’t imply that the reverse is true: $p_1 \text{ trusts } p_2 \centernot \implies p_2 \text{ trusts } p_1$.
- Trusted $(1)$ or untrusted $(0)$ is the result of the decision-making process $T_\chi(p_1, p_2)$ performed by the decision-making party $p_1$ on a promise $\chi$ and using a risk threshold $\tau_\chi$ as a decision criterion.

It’s important to note that the risk assessment is rarely an abrupt binary decision; risk is usually expressed by a combination of [heuristic](https://en.wikipedia.org/wiki/Heuristic) $r_n \circ \dots \circ r_1$ in terms of [risk sources](https://www.iso.org/obp/ui/en/#iso:std:iso:31000:ed-2:v1:en:term:3.4), [potential events](https://www.iso.org/obp/ui/en/#iso:std:iso:31000:ed-2:v1:en:term:3.5), their [consequences](https://www.iso.org/obp/ui/en/#iso:std:iso:31000:ed-2:v1:en:term:3.6)  and their [likelihood](https://www.iso.org/obp/ui/en/#iso:std:iso:31000:ed-2:v1:en:term:3.7).

$$T_\chi(p_1,p_2) = \begin{cases}
\text{1} & \text{if } r_n \circ \dots \circ r_1 (p_1,p_2, \chi) > \tau_\chi \\
\text{0} & \text{else}
\end{cases}$$

This also means that the same decision-making party $p_1$, having access to the same available information about target party’s $p_2$ capabilities, might make different final trust decisions, depending on the promises $\chi_1, \chi_2, \dots$.

The key element of the this document is the methodology and technical specifications for collecting and organising the information to perform this risk assessment in an objectif, rational and reproductible way.

!!! note
    A party $p \in P$, which can be either a $P = \{$ legal entity, natural person, process $\}$, is always uniquely identifiable.

    $$\forall p_1,p_2 \in P, \text{if } p_1 \ne p_2 \text{ then } Id(p_1) \ne Id(p_2)$$

For the decision-making party to perform a risk assessment, information, referred later as claims, about the target party, its capabilities, and the promise, referred later as the objects of the assessments, must be collected.

To organise the collected claims, this document relies on [asymmetric cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography) and [linked data](https://www.w3.org/DesignIssues/LinkedData.html) to:

- build a machine-readable knowledge graph of claims about the objects of assessment.
- be able to verify at any time the content integrity of the claims.
- keep track from where the claims originated from or to keep track of the parties issuing the claims, referred later as the issuers.

Then, to have organisational and semantic interoperability, this document provides:

- generic information models such as the Licensor-Licensee,  Producer-Provider or Provider-Consumer relations, the [law of obligations](https://en.wikipedia.org/wiki/Law_of_obligations), the contracting procedures, and rules.
- data exchange specific information models such as data transactions, data intermediaries, consent management for data processing, policy management.
- vocabularies and schemata to describe the characteristics of the objects of the assessments, and enable policy reasoning on those characteristics [^10.1007/978-3-030-17294-7_4] [^10.1007/978-3-030-31095-0_3], like ICT services, cloud offerings like big data cluster, AI training and inferencing engines, hardware and software infrastructure including edge devices, data product, the licence and terms of uses.

[^10.1007/978-3-030-17294-7_4]: [Using Semantic Web Technologies and Production Rules for Reasoning on Obligations and Permissions](https://doi.org/10.1007/978-3-030-17294-7_4) by Fornara, Nicoletta and Chiappa, Alessia and Colombetti, Marco.
[^10.1007/978-3-030-31095-0_3]: [ODRL Policy Modelling and Compliance Checking](https://doi.org/10.1007/978-3-030-31095-0_3) by De Vos, Marina and Kirrane, Sabrina, and Padget, Julian, and Satoh, Ken

To be noted that the last point introduces the topic of automated or supervised policy reasoning, which is a key element for future smart legal contract[^EU_Smart_Contract] [^10.1093/oso/9780192858467.001.0001], including legally binding contracts[^Contract_Law].

[^EU_Smart_Contract]: [Smart contracts and the digital single market through the lens of a “law plus technology” approach](https://digital-strategy.ec.europa.eu/en/library/smart-contracts-and-digital-single-market-through-lens-law-plus-technology-approach) by Thibault Schrepel
[^10.1093/oso/9780192858467.001.0001]: [Smart Legal Contracts: Computable Law in Theory and Practice](https://doi.org/10.1093/oso/9780192858467.001.0001) by Allen, Jason and Hunn, Peter
[^Contract_Law]: [Contract Law: A Comparative Introduction](https://www.e-elgar.com/shop/gbp/contract-law-9781800373129.html) by Jan Smits

This framework also enables both parties $p_1$ and $p_2$ to exchange their roles where $p_2$ becomes the decision-making party and $p_1$ the target party.

!!! tip

    For example, in a context where $p_2$ provides data processing services, $p_1$ might want to ensure that the services provided by $p_2$ meet its needs in terms of legal, operational and technical autonomy and $p_2$ might want to ensure that $p_1$ will use the service and process the data in accordance with the agreed terms of service and data processing purposes.

The next sections reuse as much as possible of the existing vocabularies and terms defined in:

- The [W3C Verifiable Credentials](https://w3c.github.io/vc-data-model/) and [Linked-Data](https://www.w3.org/wiki/LinkedData) standards.
- The vocabulary and general principles from the [ISO Committee on Conformity Assessment (ISO/CASCO)](https://www.iso.org/obp/ui/en/\#iso:std:iso-iec:17000:ed-2:v2:en)
