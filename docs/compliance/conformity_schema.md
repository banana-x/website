# Conformity schema

!!! tip Prerequisite

    The reader is invited to read the [Conformity model](../fundamentals/conformity.md) first.

To help a decision-making party with its assessment, the [ecosystem authority](../ecosystem/authority.md) can predefine assessments scheme:

- some could be mandatory to enforce a minimal set of [requirements](https://www.iso.org/obp/ui/\#iso:std:iso-iec:17000:ed-2:v2:en:term:5.1) for an object to be qualified as compliant in the ecosystem.
- other could be optional to let ecosystem parties distinguish from each other, or be domain specific, or fit for a specific legislation.

In the context of globalisation and foster [legal and organisational interoperability](../ecosystem/interoperable_ecosystems.md), it’s important to note that it’s up to the ecosystem authority to be innovative and articulate requirements that capture the intent of a legislation without necessarily referring to it.

!!! example "Example of Regulation vs Intent of the regulation"

    Given the following two requirements with similar objectives:

    1. _"Regardless of its location and the location of the service users, a service provider must comply with [EU General Data Protection Regulation (GDPR)](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX\%3A32016R0679)."_
    2. _"If personal or sensitive data is processed by a service provider, the later must always be able to prove, on request, that the owner of the data has given unequivocal consent for its processing and for explicit purposes and duration."_

    The first one adds little value to EEA providers and EEA residents, because the GDPR is already enforced by law.
    However it adds a significant burden on providers not initially subject to EU law and surely slows down the ecosystem interoperability where similar data protection regulations are already in place: [The Act on the Protection of Personal Information Act No. 57 of 2003 (Japan :flag_jp:)](https://www.cas.go.jp/jp/seisaku/hourei/data/APPI.pdf), [General Personal Data Protection Law (Brazil :flag_br:)](http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2018/lei/l13709.htm), [Personal Data Protection Act 2012 (Singapore :flag_sg:)](https://sso.agc.gov.sg/Act/PDPA2012) , [California Consumer Privacy Act (USA :flag_us:/California)](https://oag.ca.gov/privacy/ccpa) , [Virginia Consumer Data Protection Act(USA :flag_us:/Virginia](https://lis.virginia.gov/cgi-bin/legp604.exe?212+ful+CHAP0036+pdf), $\dots$

    The second one offers the ecosystem autority the opportunity to further detail the semantics and syntax of the claims to be collected to demonstrate compliance with the requirement, such as consent, processing purposes, duration, service provider contact point for information requests, revocation of consent and so on, to reach semantic interoperability across existing data protection regulations.

This extra effort by the ecosystem authority to detail as much as possible the semantics and syntax of the information required to provide evidence that a requirement is fulfilled is directly proportional to the future effectiveness of organisational and semantic interoperability.

## Validation vs Verification

To help the ecosystem authority, a decision flow chart, has been developed in accordance with the ISO/CASCO and W3C Verifiable Credentials standards, starting from a high-level criterion down to measurable and comparable requirements.

<!-- \ref{fig:criteria-workflow} -->

This workflow is summarised as follows:

  - For each requirement, the Gaia-X policy-rules makers must decide if the claims must be validated or verified.
    - For a [validation](https://www.iso.org/obp/ui/\#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5) , a $1^{st}$ or $2^{nd}$ party assessment activity is sufficient.
    - For [verification](https://www.iso.org/obp/ui/\#iso:std:iso-iec:17000:ed-2:v2:en:term:6.6), a $3^{rd}$ party assessment activity is required.
  - For each claim, the Gaia-X policy-rules makers must specify one or more of the following lists:
    - The accepted claim’s issuers.
    - The characteristics qualifying an accepted issuer.
    - The characteristics qualifying an accepted public-key to digitaly sign the claim.

Those lists or the results of the evaluation of the characteristics from those lists are referred as [Trust Anchors](./trust_anchors.md) and are published via the ecosystem Registry service.

The careful selection of the Trust Anchors is critical for the result of the assessment to be legally relevant or legally binding.

Finally, a special attention from the ecosystem authority must be given to the [scope](https://www.iso.org/obp/ui/\#iso:std:iso-iec:17000:ed-2:v2:en:term:7.4) of the collected claims.

For each requirement $r$, the union of the scopes $s(c_i)$ of the collected claims $c_i$ must cover the entire scope $s(r)$ of the requirement $r$, such as $s(r) = s(r) \cap \Big( \bigcup_i^ns(c_i) \Big)$ -->

## Composition of conformity assessment scheme

!!! warning

    A conformity scheme can be composed as described below under the condition that the scopes of the object of conformity `#1` and `#2` are indisputable between the conformity scheme.

In the diagram below, the `Conformity Assessment System #1` is composed of the `Conformity Assessment System #2`.

The `Conformity Assessment System #2` has no impact on `Conformity Assessment System #1`.

!!! example

    The `Conformity Assessment Activities #1` could be fully automated while the `Conformity Assessment Activities #2` could be manual.


```mermaid
flowchart

    subgraph CASystem [Conformity Assessment System #1]
        direction TB
        CAScheme(Conformity Assessment Scheme #1)
        object(Objects of Conformity Assessment #1)
        criteria(Specified Requirements #1)
        CAB(Conformity Assessment Bodies #1)
        CAA(Conformity Assessment Activities #1)

        CAScheme -- identify --> criteria
        CAScheme -- provide the methodology to perform --> CAA
        CAA -- demonstrate fulfillment of --> criteria
        CAB -- perform --> CAA

        criteria -- might refers to 3rd party --> CAScheme2

        subgraph CASystem1 [Conformity Assessment System #2]

            CAScheme2(Conformity Assessment Scheme #2)
            criteria2(Specified Requirements #2)
            CAB2(Conformity Assessment Bodies #2)
            CAA2(Conformity Assessment Activities #2)
            object2(Objects of Conformity Assessment #2)

            CAB2 -- perform --> CAA2
            CAScheme2 -- identify --> criteria2
            CAScheme2 -- provide the methodology to perform --> CAA2
            CAA2 -- demonstrate fulfillment of --> criteria2
            criteria2 -- need or expectation applying to --> object2

        end
            object2 -- equivalent or part of --> object
    end
```

!!! example

    For a given ecosytem or domain specific qualification or label, the scheme owner can define a `Conformity Scheme #1` that mandates the acquisition of an ISO/IEC 27001 from a specific list of eligible `CAB #2`.
    Those CAB are a type of [Trust Anchors](./trust_anchors.md).