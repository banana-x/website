# :fontawesome-solid-signature: Signature

!!! warning "Electronic vs Digital signature"
    An **electronic** signature, serving as a digital manifestation of an individual's agreement to a document or contract, may encompass diverse formats such as scanned handwritten signatures, typed names, or recorded voices, yet it does not inherently verify document content, nor ensure security and legal recognition, as the assurance levels can vary legally.

    A **digital** signature, a subtype of electronic signature, employs cryptographic techniques to generate a digital certificate, offering evidence of identity verification; through the creation of a distinctive digital fingerprint, digital signatures ensure the utmost security assurance by authenticating the signer's identity via a trusted third party, referred to as a Certificate Authority (CA).


In this document, signatures are always digital signatures unless otherwise specified.
