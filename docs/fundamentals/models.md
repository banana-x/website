# :simple-diagramsdotnet: Models

This specification is leveraging several existing models for its architecture. Those will be reused later in the document.

!!! success "Nesting and recursive models"

    The models used here can be used recursively and can be nested, to adapt to the various needs without having to envision all the possible domain specific usecases.

## :octicons-workflow-16: ArchiMate(r)

The model is inspired by the [ArchiMate 3 metamodel](https://pubs.opengroup.org/architecture/archimate3-doc/ch-Generic-Metamodel.html), and most specifically, by:

- [Passive Structure Element](https://pubs.opengroup.org/architecture/archimate3-doc/ch-Generic-Metamodel.html#sec-Passive-Structure-Elements) which is manufactured/grown by `Producers`, like `Data Product`.

- [External Behaviour Element](https://pubs.opengroup.org/architecture/archimate3-doc/ch-Generic-Metamodel.html#sec-Behavior-Elements) which is made available by `Providers`, like `Service`.

- [Active Structure Elements](https://pubs.opengroup.org/architecture/archimate3-doc/ch-Generic-Metamodel.html#sec-Active-Structure-Elements) which are used to detail `Service` composition.

!!! tip "Provider and Producer"

    *Passive Structure Element*, like a `Data Product` from a `Data Product Producer`, can **only** be accessed via an *External Behavior Element*, like a `Service` operated by a `Service Provider`.

!!! warning "Provider and Producer"

    As a consequence of the above definition, should there be compliance rules for `Data Product` exchange, those rules shall apply to the `Service` and `Service Provider` too.

As general notes:

1. Data is static, is does nothing by itself.
2. To be stored, transfered or processed, data require services as actively running software or computer code.
3. Policies, access controls, monitoring, auditing, $\dots$ can only be performed on services

=== "Asset model - simple"

    ![Asset model](images/composition-simple.svg)

=== "ArchiMate - Generic Metamodel"

    ![Behavior and Structure Elements Metamodel (ArchiMate)](https://pubs.opengroup.org/architecture/archimate3-doc/images/fig-Behavior-and-Structure-Elements-Metamodel.png)


Each of the Resource, Service Offer, Request and Agreement has attached Rules

<!-- TODO:add resource SO, request, agreement as Assets and appli rules to assets -->


## :octicons-workflow-16: Open Digital Rights Language Information Model

This section is rather short but extremely important and the reader is highly encouraged to read the linked ODRL specifications.

!!! success "Policy expression language"

    This specification recommends to use [ODRL Information Model](https://www.w3.org/TR/odrl-model/) and [ODRL Vocabulary](https://www.w3.org/TR/odrl-vocab/) to express policies.

The policies are directly expressed and embedded in VC(s) as rules and can be about:

- `Data Product` and `Service` access and usage policies
- requests, data offers and agreements
- basic regulatory policies
- VC terms of use, rights delegation between parties

The specific attribute names to store policies are specified in this ecosystem Ontology.

=== "ODRL Information Model - simple"

    ![ODRL Information Model](images/odrl-simple.svg)

=== "ODRL Information Model - complete"

    ![ODRL Information Model](https://www.w3.org/TR/odrl-model/00Model.svg)


### ODRL as intermediate language

```mermaid
flowchart LR

eFlint --> ODRL
Cicero --> ODRL
LegalRuleML --> ODRL
t1["..."] --> ODRL

ODRL --> Rego
ODRL --> Ergo
ODRL --> IAM
ODRL --> SPARQL
ODRL --> t2["..."]
```

!!! info "Extra reading"

    ??? quote ":material-newspaper-variant-outline: ODRL Policy Modelling and Compliance Checking"

        > De Vos, M., Kirrane, S., Padget, J., Satoh, K. (2019). [ODRL Policy Modelling and Compliance Checking](https://penni.wu.ac.at/papers/RuleML%20RR%202019%20ODRL%20policy%20modelling%20and%20compliance%20checking.pdf). In: Fodor, P., Montali, M., Calvanese, D., Roman, D. (eds) Rules and Reasoning. RuleML+RR 2019. Lecture Notes in Computer Science(), vol 11784. Springer, Cham. https://doi.org/10.1007/978-3-030-31095-0_3
    

    ??? quote ":material-newspaper-variant-outline: Using Semantic Web Technologies and Production Rules for Reasoning on Obligations and Permissions"

        > Fornara, N., Chiappa, A., Colombetti, M. (2019). [Using Semantic Web Technologies and Production Rules for Reasoning on Obligations and Permissions](https://penni.wu.ac.at/papers/RuleML%20RR%202019%20ODRL%20policy%20modelling%20and%20compliance%20checking.pdf). In: Lujak, M. (eds) Agreement Technologies. AT 2018. Lecture Notes in Computer Science(), vol 11327. Springer, Cham. https://doi.org/10.1007/978-3-030-17294-7_4
