# :material-link: Linked Data, RDF and RDF-star

In this specification, all the information is organised following the Linked Data principles and RDF data model.

Describing [RDF specifications](https://www.w3.org/TR/rdf11-concepts/) is out of the scope of that document and  the core concepts are:

- the structure of the information is a set of RDF triples, called a RDF graph.
- each RDF triple consists of 3 components: the `subject`, the `predicate`, the `object`.
- each component is a IRI. The `object` can also be a literal.

```mermaid
flowchart LR
    s("Subject")
    o("Object")
    s -- Predicate --> o
```

??? example "Example of the same LAMP service"


    ```mermaid
    flowchart TB

    classDef provider fill:#b8a500
    classDef vitresource fill:#ffec3d
    classDef phyresource fill:#ffee57
    classDef serviceoffering fill:#fa0
    classDef instvitresource fill:#fbc02d

    so{{Service Offering}}:::serviceoffering
    pr[/Physical Resource\]:::phyresource
    vr[\Virtual Resource\]:::vitresource
    ivr[\Instantiated Virtual Resource/]:::instvitresource
    part[Participant]:::provider
    ```

    Same [LAMP](https://en.wikipedia.org/wiki/LAMP_%28software_bundle%29) service offering, with different number of interoperable providers.

    === "Example with 11 different providers"

        Service with a high level of composability, with multiple interoperable providers.

        ```mermaid
        flowchart TB

        classDef provider fill:#b8a500
        classDef vitresource fill:#ffec3d
        classDef phyresource fill:#ffee57
        classDef serviceoffering fill:#fa0
        classDef instvitresource fill:#fbc02d

        wh -- providedBy --> prov1
        wh -. aggregationOf .-> lamp

        lamp -. aggregationOf .-> paas
        lamp -- copyrightOwnedBy --> softed1
        lamp -- tenantOwnedBy --> prov1

        lamp -- maintainedBy --> prov2

        server -- manufacturedBy --> manu1
        server -. aggregationOf .-> dc
        server -- onwedBy --> owner1
        server -- maintainedBy --> prov4

        dc -- manufacturedBy --> manu2
        dc -- onwedBy --> owner2
        dc -- maintainedBy --> prov5

        paas -. aggregationOf .-> server
        paas -- maintainedBy --> prov3
        paas -- copyrightOwnerBy --> softed2
        paas -- tenantOwnedBy --> prov2
        paas -. aggregationOf .-> conf

        conf -- copyrightOwnerBy --> prov3
        
        wh{{Web Hosting}}:::serviceoffering
        paas[\Platform as a Service/]:::instvitresource
        prov1[National Provider]:::provider

        lamp[\LAMP/]:::instvitresource
        server[/Servers\]:::phyresource
        dc[/Datacenter\]:::phyresource
        conf[\Configuration\]:::vitresource

        softed1[Non-National Software Editor]:::provider
        prov2[Provider 2]:::provider

        softed2[Software Editor 2]:::provider
        prov3[Provider 3]:::provider

        manu1[Manufacturer 1]:::provider
        owner1[Owner 1]:::provider
        prov4[Provider 4]:::provider

        manu2[Manufacturer 2]:::provider
        owner2[Owner 2]:::provider
        prov5[Provider 5]:::provider
        ```

    === "Example with 4 different providers"

        Service with a low level of composability, articulated around few partners.

        ```mermaid
        flowchart TB

        classDef provider fill:#b8a500
        classDef vitresource fill:#ffec3d
        classDef phyresource fill:#ffee57
        classDef serviceoffering fill:#fa0
        classDef instvitresource fill:#fbc02d

        wh -- providedBy --> prov1
        wh -. aggregationOf .-> lamp

        lamp -. aggregationOf .-> paas
        lamp -- tenantOwnedBy --> prov1

        lamp -- maintainedBy --> prov1

        server -. aggregationOf .-> dc
        server -- onwedBy --> softed1
        server -- maintainedBy --> softed1
        server -- manufacturedBy --> manu1

        dc -- onwedBy --> softed1
        dc -- manufacturedBy ---> manu2
        dc -- maintainedBy --> softed1

        paas -. aggregationOf ..-> server
        paas -. aggregationOf .-> conf
        paas -- maintainedBy --> prov1
        paas -- copyrightOwnerBy --> softed1
        paas -- tenantOwnedBy --> prov1

        conf -- copyrightOwnerBy --> softed1
        lamp -- copyrightOwnedBy --> softed1
        
        wh{{Web Hosting}}:::serviceoffering
        paas[\Platform as a Service/]:::instvitresource
        prov1[National Provider]:::provider
        lamp[\LAMP/]:::instvitresource
        server[/Servers\]:::phyresource
        dc[/Datacenter\]:::phyresource
        conf[\Configuration\]:::vitresource
        softed1[Non-National Software Editor]:::provider
        manu1[Manufacturer 1]:::provider
        manu2[Manufacturer 2]:::provider
        ```

## Serialisation

RDF is a data model which has several serialisation formats such as Turtle, JSON-LD, RDF/XML, and others. All the below examples represent the data graph and have the same semantics.

=== "Turtle (.ttl)"

    ```turtle
    @prefix tim: <https://www.w3.org/People/Berners-Lee/>.
    @prefix schema: <http://schema.org/>.
    @prefix dbpedia: <http://dbpedia.org/resource/>.

    <tim> schema:birthDate "1955-06-08"^^<http://www.w3.org/2001/XMLSchema#date>.
    <tim> schema:birthPlace <dbpedia:London>.
    ```

=== "JSON-LD (.json, .jsonld)"

    ```json
    {
      "@context": {
        "dbpedia": "http://dbpedia.org/resource/",
        "schema": "http://schema.org/"
      },
      "@id": "https://www.w3.org/People/Berners-Lee/",
      "schema:birthDate": "1955-06-08",
      "schema:birthPlace": {
        "@id": "dbpedia:London"
      }
    }
    ```

=== "RDF/XML (.xml)"

    ```xml
    <?xml version="1.0"?>
    <rdf:RDF  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"  xmlns:schema="http://schema.org/">
      <rdf:Description rdf:about="https://www.w3.org/People/Berners-Lee/">
        <schema:birthDate>1966-06-08</schema:birthDate>
        <schema:birthPlace rdf:resource="http://dbpedia.org/resource/London"/>
      </rdf:Description>
    </rdf:RDF>
    ```

=== "N-Triples (.nt), N-Quads (.nq)"

    ```xml
    <https://www.w3.org/People/Berners-Lee/> <http://schema.org/birthDate> "1955-06-08"^^<http://www.w3.org/2001/XMLSchema#date>.
    <https://www.w3.org/People/Berners-Lee/> <http://schema.org/birthPlace> <http://dbpedia.org/resource/London>.
    ```

## Normalisation / Canonisation

An RDF data graph can be normalised or canonised to produce the same result as long as the input data graphs have the same semantic meaning. Any input serialisation format can be used.

All the graphs above in the [Serialisation chapter](#serialisation) will produce the same signature after [URDNA2015](https://w3c-ccg.github.io/rdf-dataset-canonicalization/spec/) or [RDFC-1.0](https://www.w3.org/TR/rdf-canon) normalisation / canonisation.

For example, if we take these two RDF data graphs represented as JSON-LD with fields in a different order.

=== "Example 1"

    ```json
    {
      "@context": {
        "dbpedia": "http://dbpedia.org/resource/",
        "schema": "http://schema.org/"
      },
      "@id": "https://www.w3.org/People/Berners-Lee/",
      "schema:birthDate": "1955-06-08",
      "schema:birthPlace": {
        "@id": "dbpedia:London"
      }
    }
    ```

=== "Example 2"

    ```json
    {
      "@context": {
        "schema": "http://schema.org/",
        "dbpedia": "http://dbpedia.org/resource/"
      },
      "@id": "https://www.w3.org/People/Berners-Lee/",
      "schema:birthPlace": {
        "@id": "dbpedia:London"
      },
      "schema:birthDate": "1955-06-08"
    }
    ```

They will produce the same result when normalised / canonised with the [URDNA2015](https://w3c-ccg.github.io/rdf-dataset-canonicalization/spec/) algorithm with an [N-Quads](https://www.w3.org/TR/rdf12-n-quads/) output format.

=== "Result"

  ```xml
  <https://www.w3.org/People/Berners-Lee/> <http://schema.org/birthDate> "1955-06-08" .
  <https://www.w3.org/People/Berners-Lee/> <http://schema.org/birthPlace> <http://dbpedia.org/resource/London> .
  ```

## RDF-star

Instead of using RDF reification, this specification use [RDF-star](https://www.w3.org/2021/12/rdf-star.html) or RDF* to associate information to the `predicate`.  
This is especially useful to add context about who made the claim.

!!! success "RDF-start serialisation"
    
    Usage of [Turtle-star](https://www.w3.org/2021/12/rdf-star.html#turtle-star) is recommended to serialise RDF-star.


!!! warning "RDF-star vs Property Graph"

    Property graph (PG) allows to add properties to an edge, but not to associate an edge to another node of the graph.  
    Example: in case two VC are issued by the same issuer, a PG would duplicate the issuer information on every edge, making the long term maintainance of the graph and search impracticable, while RDF* will associate the information to a specific node.
