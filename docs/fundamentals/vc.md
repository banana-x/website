# :material-link-lock: Verifiable Credentials

To share, exchange, transport the information between entities, the claim(s) about a subject(s) are encapsulated into [Verifiable Credentials](https://www.w3.org/TR/vc-data-model-2.0/) which provide:

- Cryptographic mechanism to prove that the claim(s) were not tampered.
- Information related to identifying the issuing authority (for example, a city government, national agency, natural person, CAB).

The VC data model adds additional information related to:

> - identifying the subject of the credential (for example, a photo, name, or identification number).
> - the type of credential this is (for example, a Dutch passport, an American driving license, or a health insurance card).
> - to specific attributes or properties being asserted by the issuing authority about the subject (for example, nationality, the classes of vehicle entitled to drive, or date of birth).
> - how the credential was derived.
> - constraints on the credential (for example, validity period, or terms of use).

## Role model

??? quote "For reference from the W3C VC specifications"

    ![The roles and information flows forming the basis for W3C VC specification.](https://www.w3.org/TR/vc-data-model-2.0/diagrams/ecosystem.svg)

VC(s) are managed via a wallet, a registry or an agent service run by the holder or by a party the holder has delegated rights to - custodian wallet.

!!! success "Design principle"

    The holder is **always** in control with whom or what the VC are exchanged.  
    If the holder decides to delegate the management of its wallet to a 3rd party, it's still a decision from the holder.

While protocols to exchange VC are still under development, the protocols shall:

- allow the holder to assess the verifier identity to avoid spoofing attack.
- allow the verifier to assess the holder identity to avoid Man-in-the-Middle (MITM) attack.
- prevent replay attacks, using a cryptographic nonce during the presentation.
- enable the holder to assess what claims are being asked, using [JSON-LD Framing](https://w3c.github.io/json-ld-framing/) as part of the [Presentation Exchange](https://identity.foundation/presentation-exchange/spec/v2.0.0/) for example.

!!! success "Design principle"

    To easy the exchange of VC, it's expected for the VC identifier URL to be resolvable to a service endpoint which can implement access and usage control.  
    Should the URL not to be resolvable, it's the responsability of the holder to find a mean to exchange the VC(s).

## Issuance

??? quote "For reference from the W3C VC specifications"

    ![Example of flow diagram for VC issuance.](https://www.w3.org/TR/vc-use-cases/uc-issuing-claims.svg)

!!! info "Self issued VC"
    
    In case the holder is the issuer, the VC is said to be self-issued. It's a self-declaration or self-description.

## Lifecycle

??? quote "For reference from the W3C VC specifications"

    ![The roles and information flows for the W3C VC specification](https://www.w3.org/TR/vc-data-model-2.0/diagrams/ecosystemdetail.svg)
