# :material-certificate: Conformity Assessment

This section is based on the [ISO/IEC 17000:2020 Conformity assessment](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en) by the ISO Committee on Conformity Assessment, [CASCO](https://www.iso.org/casco.html)

!!! warning "Compliance vs Compatibility"

    Compliant service is about conforming to given policies or rules.  
    Compatible software is about conforming to given technical implementation.  
    Two services can be compliant to the same policies and rules but not necessarily compatible.  
    Two services can be compatible with each other but not necessarily compliant to the same policies and rules.


## Workflows

```mermaid
flowchart

    SystemOwner(System Owner)
    subgraph CASystem [Conformity Assessment System]
        direction TB
        SchemeOwner(Schema Owner)
        CAScheme(Conformity Assessment Scheme)
        object(Objects of Conformity Assessment)
        criteria(Specified Requirements)
        AB(Accreditation Bodies)
        CAB(Conformity Assessment Bodies)
        CAA(Conformity Assessment Activities)

        SchemeOwner -- develop and maintain --> CAScheme
        CAScheme -- identify --> criteria
        CAScheme -- provide the methodology to perform --> CAA
        CAA -- demonstrate fulfillment of --> criteria
        criteria -- need or expectation applying to --> object
        CAB -- perform --> CAA
        AB -- issue  accreditations to --> CAB
        SchemeOwner -. award authority to .->  AB
    end

    SystemOwner -- develops and maintains --> CASystem
```

The positive result of the conformity assessment activities is an attestation, which is a statement that fulfilment of Specified Requirements has been demonstrated, for a given Conformity Assessment Scheme and [Scope of attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.4).

## Attestation types

Type of [Attestations](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3) in ISO/IEC 17000:2020 | Example
------------------------------------------------------------------------------------------------------------------|---------
[first-party conformity assessment activity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.3), also known as [declaration](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.5) | a person **self-declaring** itself competent
[second-party conformity assessment activity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.4) | assessment of a person's knowledge and skills conducted by someone **with the interest** of the person (trainer/instructor).
[third-party conformity assessment activity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.5), also known as [certification](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.6) | assessment of a person's knowledge and skills conducted via a **impartial** exam.


<!-- https://www.proftesting.com/blog/2016/09/28/first-second-and-third-party/ -->

## Assessment and Signing activities

The ISO/IEC 17000:2020 and VC documents mentioned above have overlapping and different scopes.


| ISO/IEC 17000:2020 term                                                        | | Verifiable Credentials Data Model term                                       |
|--------------------------------------------------------------------------------|-|------------------------------------------------------------------------------|
| :green_square: The [object](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.2) of the assessment | is equivalent to| :green_square: The [RDF subject](https://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#dfn-subject) in the [credentialSubject](https://www.w3.org/TR/vc-data-model-2.0/#credential-subject) |
| :green_square: The [CAB](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6) issuing an attestation | is equivalent to | :green_square: The VC [issuer](https://www.w3.org/TR/vc-data-model-2.0/#issuer) |
| :orange_square: An [attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3) document (.pdf, .jpg) doesn't necessarily contain cryptographic mechanism to be tamper-proof.  | is NOT equivalent to | :green_square: A VC must contain at least one cryptographic [proof](https://www.w3.org/TR/vc-data-model-2.0/#proofs-signatures) |
| :green_square: An [attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3) document must refer to a [Conformity Assessment Scheme](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.9).  | is NOT equivalent to | :orange_square: The VC's claims are issued under the sole authority of the issuer. |

!!! success

    This specification is leveraging the best :green_square: of both ISO/IEC 17000:2020 and W3C VC specifications by:

    - Adopting the formal and well defined ISO/IEC 17000:2020 workflows for issuing claims.
    - Adopting the state-of-the-art W3C VC mechanism to ensure tamper-proof and machine readable information. 

!!! example
    
    1. A CAB outputs attestation(s).
    2. A VC issuer issues credential(s).
    3. A [Trust Service Provider](https://en.wikipedia.org/wiki/Trust_service_provider) is a type of CAB which issues certificate(s).
    4. A CAB can also issue credential(s).
    5. The signature of a credential can be verified using the certificate of the credential's issuer.
    6. The trustworthiness of a credential is directly proportional to the capacity of the verifier to assess the trustworthiness of the issuer's certificate.
