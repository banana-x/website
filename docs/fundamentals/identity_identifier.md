# :material-badge-account-horizontal-outline: Identity and Identifier

!!! warning "Identity vs Identifier"

    A subject can have several identities, each of them with a different identifier.

The key elements of the identity trustworthiness are:

- The demonstration that the holder has the control and legitimacy to present the VC(s).
- The issued identity's VC(s) are conformant with the [KYB/KYC](https://en.wikipedia.org/wiki/Know_your_customer) rules associated with the issuance of the identifiers, for legal entities, natural persons and workloads. ([eIDAS](https://digital-strategy.ec.europa.eu/en/policies/eidas-regulation), [SPIFFE](https://spiffe.io/), ...)
- The legitimacy of the TSP based on the information available in the [Verifiable Data Registries](https://www.w3.org/TR/vc-data-model/#dfn-verifiable-data-registries).

!!! note "Design principle"

    To be noted that any party identifiers mentionned in any of the named graphs below - _Presentation Graph_, _Credential Graph_, _Claim_, _Credential Proof Graph_, _Presentation Proof Graph_ - can potentially be dereferenced to a VC containing information about the party identity.  
    This is a highly nested model.

    ![Information graphs associated with a basic verifiable presentation.](https://www.w3.org/TR/vc-data-model/diagrams/presentation-graph.svg)

## X.509 and DID + VC

In essence, a X.509 certificate is the composition of one DID document with one or more VC.

<!-- TODO: include graph like https://miro.medium.com/v2/resize:fit:2404/1*qipblkoxrjx1EtPXBfQBJQ.png -->
