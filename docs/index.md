# :book: Introduction

Banana-X 🍌 is a pure fictional corporation that aim to create cheap, scalable, reliable, secure, privacy preserving, extendable, lock-in and lock-out free solutions for data exchange and service composition.

This document is intended to be a pragmatic and concise description for:

- building a global network of catalogues for services and data products.
- enabling trusted data transaction at scale
- setting up interoperable governance across different domain(s).

<!-- - the Banana-X Compliance as 2nd order logic rules on the Banana-X ontology.
- the Banana-X Ontology as a model understandable by an algorithm to describe Banana-X entities such as and not limited to Participant, ServiceOffering, Resource, DataProduct, ... -->

## :octicons-cross-reference-16: External references

When references to external documents or resources are made, it's expected from the readers to follow the links and get familiar with the content.

## :fontawesome-solid-language: Language

While not all authors are native English speakers, the official language of this document is English UK.

In case a word is unknown or unclear, please use the dictionaries below:

- [Cambridge Dictionary](https://dictionary.cambridge.org/dictionary/)
- [Collins Dictionary](https://www.collinsdictionary.com/dictionary/)
- [Oxford Learner's Dictionary](https://www.oxfordlearnersdictionaries.com/definition/)
