# :material-account-tie-hat: Ecosystem Authority

The ecosystem is consisted of several parties offering and requesting goods and services.  
Each party can have one or multiple roles.

!!! warning "Party vs Person vs Participant"

    A party is not limited to be a legal or natural person/participant. It can also be a workload, ie a running application/program.

    Example of workload identity management: [SPIFFE/SPIRE](https://spiffe.io/)

## Authority

The role of the ecosystem authority is to define, operationalise and monitor the rules of the ecosystem.

!!!tip "How to identity the authority ?"

    Look for the parties defining the ecosystem onboarding rules.
    If there is no onboarding rule, identity the parties accountable for the litigations.

    **Warning**: the parties defining the rules are not necessarly the ones enforcing them.

## Provider and Consumer

The Provider and Consumer roles constitute the basics of the ecosystem.

```mermaid
flowchart LR
    provider(Provider)
    consumer(Consumer)

    provider -- offers to --> consumer
    consumer -- requests to --> provider
```

The output of the negotiation between a provider and a consumer is a contract governed by four principles:

- freedom of contract
- binding force
- the absence of formalities
- contractual fairness

The provider and consumer are the contracting parties.

<!-- This very generic building block can be nested and recursive to build more complex blueprints and conceptual models, from DSSC DataSpace Authorities or DGA Data Intermediaries to bubble gum vending machines. -->

!!! info "Extra reading"

    ??? quote ":material-newspaper-variant-outline: Contract Law, A Comparative Introduction"

        > [Contract Law: A Comparative Introduction - 3rd edition](https://www.e-elgar.com/shop/gbp/contract-law-9781800373129.html)  
        > Jan M. Smits, Professor of Private Law, Faculty of Law, Maastricht University, the Netherlands 

    ??? quote ":material-newspaper-variant-outline: Smart contracts and the digital single market through the lens of a “law + technology” approach"

        > European Commission, Directorate-General for Communications Networks, Content and Technology, Schrepel, T., [Smart contracts and the digital single market through the lens of a “law + technology” approach](https://data.europa.eu/doi/10.2759/562748), Publications Office of the European Union, 2021, https://data.europa.eu/doi/10.2759/562748

