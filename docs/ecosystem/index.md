# :material-earth: Federation of Ecosystems

Banana-X 🍌 is a blueprint to build interoperable ecosystems.

While this ecosystem focuses on data 💾 and edge to cloud infrastructure 🛣️, this blueprint can be used for other type of goods and fields such as Digital Product Passport[^dpp] [^green_deal] for batteries 🔋, clothes 👕, cosmetics 💄, ...


[^dpp]: Digital Product Passport: <https://circulareconomy.europa.eu/platform/sites/default/files/michele-galatola-european-commission.pdf>
[^green_deal]: Green Deal: [New proposals to make sustainable products the norm and boost Europe's resource independence.](https://ec.europa.eu/commission/presscorner/detail/en/ip_22_2013)
