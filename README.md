# Banana-X docs

Online version at [Banana-X 🍌](https://www.banana-x.eu).

## Icons

Use this search engine to find icons and emojis

<https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/?h=icon#search>


## Setup for local preview

```shell
pip install -r requirements.txt
mkdocs serve --dev-addr 0.0.0.0:8000
```
