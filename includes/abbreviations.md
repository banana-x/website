*[attestation]: Unsigned output resulting from an assessement activity (ISO/IEC 17000:2020)
*[authority]: Organisation willing to help participants interact with trust
*[CAB]: Conformity Assessment Body (CASCO)
*[certificate]: Cryptographic material binding an identity to a public key using digital signature (T-REC-X.509)
*[certification]: A type of 3rd party issued attestation (ISO/IEC 17000:2020)
*[claim]: A statement about a subject, expressed as RDF triple (W3C VC)
*[contract]: legally binding agreement (EU jurisdiction)
*[credential]: Signed document containing claims about one or more subjects (W3C VC)
*[data]: Digital representation of information
*[declaration]: A type of self-issued attestation (ISO/IEC 17000:2020)
*[domain]: A domain is a particular field of thought, activity, or interest, especially one over which someone has control, influence, or rights. (Collins)
*[DPP]: Digital Product Passport
*[DSSC]: Data Spaces Support Centre
*[DA]: Data Act (EU)
*[DGA]: Data Governance Act (EU)
*[EEA]: European Economic Area
*[eIDAS]: Electronic Identification and Trust Services
*[holder]: Verifiable Credential holder (W3C VC)
*[ICT]: Information and Communication Technology
*[issuer]: A Verifiable Credential issuer (W3C VC)
*[KYB]: Know Your Business
*[KYC]: Know Your Customer
*[M2M]: Machine-to-Machine
*[MITM]: Man-in-the-Middle
*[ODRL]: Open Digital Rights Language
*[party]: An entity or a collection of entities (W3C ODRL Model)
*[procedure]: a way of doing something, especially the usual or correct way.
*[process]: actively running software or computer code.
*[RDFStar]: Resource Description Framework Star (W3C)
*[RDF]: Resource Description Framework (W3C)
*[rule]: clear and detailed information that tell you what you are allowed to do and what you are not allowed to do.
*[SHACL]: Shapes Constraint Language (W3C)
*[SPIFFE]: Secure Production Identity Framework For Everyone
*[SSI]: Self Sovereign Identity
*[TLS]: Trust-service Status List (ETSI TS 119 612)
*[trust]: the favourable response of a decision-making party who assesses the risk concerning the target party's ability to fulfil a promise.
*[TSP]: Trust Service Provider
*[URI]: Uniform Resource Identifier (URI) is a unique sequence of characters that identifies a logical or physical resource used by web technologies
*[VC]: Verifiable Credential (W3C VC)
*[VDR]: Verifiable Data Registry (W3V VC)
*[verifier]: Verifiable Credential verifier (W3C VC)
*[VP]: Verifiable Presentation (W3C VC)
*[W3C]: World Wide Web Consortium

